package br.com.itau;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Map;


public class IO {

    public static void imprimirMensagemInicial(){
        System.out.println("*********************************");
        System.out.println("***Bem-vindo ao sistema Agenda***");
        System.out.println("*********************************");
    }

    public static Map<String, String> EntradaDados(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite o nome: ");
        String nome =   scanner.nextLine();
        System.out.println("Digite o e-mail:   ");
        String email =   scanner.nextLine();
        System.out.println("Digite o telefone: ");
        String telefone =   scanner.nextLine();

        Map<String, String> dados = new HashMap<>();
        dados.put("nome", nome);
        dados.put("email", email);
        dados.put("telefone", telefone);
        return dados;
    }

}
